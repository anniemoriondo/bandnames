//Some metal words.
var words = ["death", "bone", "corpse", "fuck",
"blood", "bleed", "tooth", "hole",
"maw", "punch", "skull", "drunk",
"throne", "hammer", "fist", "smash",
"spike", "wish", "stab", "spine",
"face", "scream", "cut", "kick",
"break", "snap", "howl", "growl",
"knife", "sword", "spear", "burn",
"bash", "bang", "piss", "smack",
"kill", "grip", "slit", "vein",
"gnash", "nail", "wrath", "rage",
"flesh", "smear", "fight", "blade",
"pain", "mace", "flay", "slice",
"pound", "tongue", "wine", "crush",
"gash", "hack", "gore", "dirt",
"shriek", "thrust", "steam", "screw",
"moon", "grime", "fang", "crunch",
"crack", "glass", "steel", "rock",
"stone", "fire", "web", "night",
"mouth", "dead", "hand", "grim",
"hell", "torn", "damp", "fog",
"ice", "frost", "drool", "bite",
"rust", "leech", "cold", "wreck",
"claw", "muck", "crow", "flame",
"raven", "mould", "slime", "murder"];

//Function that capitalizes a string. Does JS really not have a capitalize method?
function capitalize (aString) {
  capitalString = aString.charAt(0).toUpperCase() + aString.slice(1);
  return capitalString;
}

//Function that reloads the page.
function reloadPage(){
	location.reload();
}

//Function that generates the band name, formatted as two compound words.
function generateName () {
  var nameWords = [];
  for (x=0; x<=3; x++){
    nameWords.push(words[Math.floor(Math.random() * 100)]);
  }
  var formattedName = capitalize(nameWords[0])
                    + nameWords[1] + " "
                    + capitalize(nameWords[2])
                    + nameWords[3];

  return formattedName;
}

//Function that generates a random rgb color.
function generateColor(){
  var colorValues = [];
  for (x=0; x<=2; x++){
    colorValues.push([Math.floor(Math.random() * 256)]);
  }
  return "rgb(" + colorValues[0] + "," + colorValues[1] + "," + colorValues[2] + ")";
}

//Our random band name.
var randomBandname = generateName();
console.log(randomBandname);

//Our random text color.
var randomColor = generateColor();
console.log(randomColor);

var nameSpan = "<span style=\" color:" + randomColor + "\">" + randomBandname + "<span\/>";

var elmBandname = document.getElementById('bandname');
elmBandname.innerHTML = nameSpan;
