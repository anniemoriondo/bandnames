[try it in browser](https://bandnames.anniemoriondo.com/) | [gitlab repo](https://gitlab.com/anniemoriondo/bandnames)

This is a metal band name generator. It is an automated version of a running gag from a D&D campaign I was in.

I'm aware most metal bands don't actually have names that follow this format. Funny ones in fiction certainly do. That said, if you get a name you like and you use it for your band, let me know. I'd be honored.


